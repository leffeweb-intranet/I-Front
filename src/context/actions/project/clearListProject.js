import { CLEAR_LIST_PROJECT } from "../../../constants/actionTypes"

export  default ()=> (dispatch)=>{
    dispatch({
        type:CLEAR_LIST_PROJECT
    })
}