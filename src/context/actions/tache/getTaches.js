import { LIST_PROJECT_LOAD, LIST_TACHE_ERROR, LIST_TACHE_SUCCES } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getTaches = (id) => (dispatch)=>{
    dispatch({
        type: LIST_PROJECT_LOAD,
    })

   axiosInstance()
    .get(`/tache/project/${id}`)
    .then((res) => {
        dispatch({
            type: LIST_TACHE_SUCCES,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: LIST_TACHE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}