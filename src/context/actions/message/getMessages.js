import { LIST_MESSAGE_ERROR, LIST_MESSAGE_LOAD, LIST_MESSAGE_SUCCES } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getMessages = (id) => (dispatch)=>{
    dispatch({
        type: LIST_MESSAGE_LOAD,
    })

   axiosInstance()
    .get(`/message/${id}`)
    .then((res) => {
        dispatch({
            type: LIST_MESSAGE_SUCCES,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: LIST_MESSAGE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}