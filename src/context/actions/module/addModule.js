import { ADD_MODULE_ERROR, ADD_MODULE_LOADING, ADD_MODULE_SUCCESS, ADD_QUESTION_ERROR, ADD_QUESTION_LOADING, ADD_QUESTION_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const addModule = (values) => (dispatch)=>{

    dispatch({
        type: ADD_MODULE_LOADING,
    })

    axiosInstance()
    .post("/appmodule",values)
    .then((res) => {
        dispatch({
            type: ADD_MODULE_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        dispatch({
            type: ADD_MODULE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}