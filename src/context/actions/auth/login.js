import { LOGIN_ERROR, LOGIN_LOADING, LOGIN_SUCCESS } from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axiosInstance";


export const login = (values) => (dispatch) =>{

    dispatch({
        type: LOGIN_LOADING,

    })
    
    axiosInstance()
    .post("/users/login",values)
    .then(res => {

        if(res.data.success){
        localStorage.token=res.data.token;
        localStorage.userID=res.data.user.id;

        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data,

        })}
        else{
        
        dispatch({
            type: LOGIN_ERROR,
            payload: res.data.message,
        })
        }
    })
    .catch(err => {
        dispatch({
            type: LOGIN_ERROR,
            payload: err ? err.response.message : CONNECTION_ERROR
        })
    })
}