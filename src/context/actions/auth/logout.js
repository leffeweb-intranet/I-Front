import { useHistory } from "react-router";
import { LOGOUT_SUCCESS } from "../../../constants/actionTypes";


export const logout = (history) => (dispatch) =>{

    dispatch({
        type: LOGOUT_SUCCESS,

    })

    localStorage.removeItem('token');
    localStorage.removeItem('userID');
}