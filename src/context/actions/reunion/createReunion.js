import {  CREATE_REUNION_ERROR, CREATE_REUNION_LOAD, CREATE_REUNION_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const createReunion= (values) => (dispatch)=>{
    dispatch({
        type: CREATE_REUNION_LOAD,
    })
    axiosInstance()
    .post(`/reunion`,values)
    .then((res) => {
        dispatch({
            type: CREATE_REUNION_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        dispatch({
            type: CREATE_REUNION_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}