import { LIST_DEVIS_ERROR, LIST_DEVIS_LOAD, LIST_DEVIS_SUCCES, LIST_FACTURE_ERROR, LIST_FACTURE_LOAD, LIST_FACTURE_SUCCES, LIST_FILE_ERROR, LIST_FILE_LOAD, LIST_FILE_SUCCES } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getFiles = (id,type) => (dispatch)=>{
    if(type=="document"){
    dispatch({
        type: LIST_FILE_LOAD,
    })
    console.log("id",id);
    console.log("type",id);
   axiosInstance()
    .get(`/file/${id}/${type}`)
    .then((res) => {
        dispatch({
            type: LIST_FILE_SUCCES,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: LIST_FILE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })}
    else if(type=="devis"){
        dispatch({
            type: LIST_DEVIS_LOAD,
        })
        console.log("id",id);
        console.log("type",id);
       axiosInstance()
        .get(`/file/${id}/${type}`)
        .then((res) => {
            dispatch({
                type: LIST_DEVIS_SUCCES,
                payload: res.data,
            })
        })
        .catch(err => {
            dispatch({
                type: LIST_DEVIS_ERROR,
                payload: err.response ? err.response.data : CONNECTION_ERROR,
            })
        })}
        if(type=="facture"){
            dispatch({
                type: LIST_FACTURE_LOAD,
            })
            console.log("id",id);
            console.log("type",id);
           axiosInstance()
            .get(`/file/${id}/${type}`)
            .then((res) => {
                dispatch({
                    type: LIST_FACTURE_SUCCES,
                    payload: res.data,
                })
            })
            .catch(err => {
                dispatch({
                    type: LIST_FACTURE_ERROR,
                    payload: err.response ? err.response.data : CONNECTION_ERROR,
                })
            })}
}