import { LIST_USER_ERROR, LIST_USER_LOADING, LIST_USER_SUCCESS, SUPER_USER_PAGE_SUCCESS } from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axiosInstance";


export const listUserInformation = (history) => (dispatch) =>{

    dispatch({
        type: LIST_USER_LOADING,
    })
    

    axiosInstance(history)
    .get(`/client`)
    .then(res => {
        if(res.data.admin){ 
        dispatch({
            type: SUPER_USER_PAGE_SUCCESS,
            payload: res.data,
        })}

        dispatch({
            type: LIST_USER_SUCCESS,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: LIST_USER_ERROR,
            payload: err.response?.data || CONNECTION_ERROR  ,
        })
    })
}