import { EDIT_USER_ERROR, EDIT_USER_LOADING, EDIT_USER_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";


export const editUserInformation = (values) => (dispatch) =>{

    dispatch({
        type: EDIT_USER_LOADING,

    })
    
    // const userID = localStorage.getItem('userID');

    axiosInstance()
    .post(`/client/`,values)
    .then(res => {
        dispatch({
            type: EDIT_USER_SUCCESS,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: EDIT_USER_ERROR,
            payload: err.response.data,
        })
    })
}