import { CLEAR_MESSAGE, CREATE_MESSAGE_ERROR, CREATE_MESSAGE_LOAD, CREATE_MESSAGE_SUCCESS, LIST_MESSAGE_LOAD, LIST_MESSAGE_SUCCES, MESSAGE_ERROR, MESSAGE_LOAD, MESSAGE_SUCCES } from "../../constants/actionTypes";

const message=(state,{payload,type})=>{
    switch (type){
        case MESSAGE_LOAD:
            return {
                ...state,
                message:{
                    ...state.message,
                    loading: true,
                    error:null
                }
            }
           
        case MESSAGE_ERROR:
                return {
                    ...state,
                    message:{
                        ...state.message,
                        loading: false
                    }
                }

                case MESSAGE_SUCCES:

                return{
                    ...state,
                    message : {
                        ...state.message,
                        loading:false,
                        data:payload
                    }
                }



         case CREATE_MESSAGE_LOAD:
                return{
                    ...state,
                    addMessage : {
                        ...state.addMessage,
                        loading:true,
                        error:null
                    }
                }
        case CREATE_MESSAGE_SUCCESS:
                return {
                    ...state,
                    addMessage:{
                        ...state.addMessage,
                        loading: false,
                        data: payload
                    }
                }
        case CREATE_MESSAGE_ERROR:
                return {
                    ...state,
                    addMessage:{
                        ...state.addMessage,
                        loading: false
                    }
                }


        case CLEAR_MESSAGE:{
            return{
                ...state,
                addMessage: {
                    ...state.addMessage,
                    loading:false,
                    error:null,
                    data:null,
                }
            }
        }
        case LIST_MESSAGE_LOAD:
            return {
                ...state,
                messages:{
                    ...state.messages,
                    loading: true,
                    error:null
                }
            }
        case LIST_MESSAGE_SUCCES:
                return {
                    ...state,
                    messages:{
                        ...state.messages,
                        loading: false,
                        data: payload
                    }
                }
        default:
            return state;
    }
}

export default message;