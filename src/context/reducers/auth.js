import { LOGIN_ERROR, LOGIN_LOADING, LOGIN_SUCCESS, LOGOUT_SUCCESS, REGISTER_ERROR, REGISTER_LOADING, REGISTER_SUCCESS } from "../../constants/actionTypes";

const auth=(state,{payload,type})=>{
    switch (type){
        case LOGIN_LOADING:
        case REGISTER_LOADING:
            return {
                ...state,
                auth:{
                    ...state.auth,
                    loading: true,
                    error:false
                }
            }

        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            return {
                    ...state,
                    auth:{
                        ...state.auth,
                        loading: false,
                        data: payload
                    }
                }

       case LOGIN_ERROR:
       case REGISTER_ERROR:
            return {
                    ...state,
                    auth:{
                        ...state.auth,
                        loading: false,
                        error: payload
                    }
                }

      case LOGOUT_SUCCESS:
            return {
                ...state,
                auth:{
                    data:null,
                    loading: true,
                    error:false
                }
            }

    
        default:
            return state;
    }
}

export default auth;