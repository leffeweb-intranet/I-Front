import { LIST_REUNION_LOAD, LIST_REUNION_SUCCES, CREATE_REUNION_SUCCESS, CREATE_REUNION_ERROR, REUNION_SUCCES, REUNION_ERROR, REUNION_LOAD, CREATE_REUNION_LOAD } from "../../constants/actionTypes";

const reunion=(state,{payload,type})=>{
    switch (type){
        case REUNION_LOAD:
            return {
                ...state,
                reunion:{
                    ...state.reunion,
                    loading: true,
                    error:null
                }
            }
           
        case REUNION_ERROR:
                return {
                    ...state,
                    reunion:{
                        ...state.reunion,
                        loading: false
                    }
                }

                case REUNION_SUCCES:

                return{
                    ...state,
                    reunion : {
                        ...state.reunion,
                        loading:false,
                        data:payload
                    }
                }



         case CREATE_REUNION_LOAD:
                return{
                    ...state,
                    addReunion : {
                        ...state.addReunion,
                        loading:true,
                        error:null
                    }
                }
        case CREATE_REUNION_SUCCESS:
                return {
                    ...state,
                    addReunion:{
                        ...state.addReunion,
                        loading: false,
                        data: payload
                    }
                }
        case CREATE_REUNION_ERROR:
                return {
                    ...state,
                    addReunion:{
                        ...state.addReunion,
                        loading: false
                    }
                }


        case LIST_REUNION_LOAD:
            return {
                ...state,
                reunions:{
                    ...state.reunions,
                    loading: true,
                    error:null
                }
            }
        case LIST_REUNION_SUCCES:
                return {
                    ...state,
                    reunions:{
                        ...state.reunions,
                        loading: false,
                        data: payload
                    }
                }
        default:
            return state;
    }
}

export default reunion;