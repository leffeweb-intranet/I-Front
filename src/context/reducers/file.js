import { CLEAR_MESSAGE, CREATE_FILE_ERROR, CREATE_FILE_LOAD, CREATE_FILE_SUCCESS, FILE_ERROR, FILE_LOAD, FILE_SUCCES, LIST_DEVIS_LOAD, LIST_DEVIS_SUCCES, LIST_FACTURE_LOAD, LIST_FACTURE_SUCCES, LIST_FILE_LOAD, LIST_FILE_SUCCES } from "../../constants/actionTypes";

const file=(state,{payload,type})=>{
    switch (type){
        case FILE_LOAD:
            return {
                ...state,
                file:{
                    ...state.file,
                    loading: true,
                    error:null
                }
            }
           
        case FILE_ERROR:
                return {
                    ...state,
                    file:{
                        ...state.file,
                        loading: false
                    }
                }

                case FILE_SUCCES:

                return{
                    ...state,
                    file : {
                        ...state.file,
                        loading:false,
                        data:payload
                    }
                }



         case CREATE_FILE_LOAD:
                return{
                    ...state,
                    addFile : {
                        ...state.addFile,
                        loading:true,
                        error:null
                    }
                }
        case CREATE_FILE_SUCCESS:
                return {
                    ...state,
                    addFile:{
                        ...state.addFile,
                        loading: false,
                        data: payload
                    }
                }
        case CREATE_FILE_ERROR:
                return {
                    ...state,
                    addFile:{
                        ...state.addFile,
                        loading: false
                    }
                }


        case CLEAR_MESSAGE:{
            return{
                ...state,
                addFile: {
                    ...state.addFile,
                    loading:false,
                    error:null,
                    data:null,
                }
            }
        }
        case LIST_FILE_LOAD:
            return {
                ...state,
                files:{
                    ...state.files,
                    loading: true,
                    error:null
                }
            }
        case LIST_FILE_SUCCES:
                return {
                    ...state,
                    files:{
                        ...state.files,
                        loading: false,
                        data: payload
                    }
                }

    
            case LIST_DEVIS_LOAD:
                return {
                    ...state,
                    devis:{
                        ...state.devis,
                        loading: true,
                        error:null
                    }
                }
            case LIST_DEVIS_SUCCES:
                    return {
                        ...state,
                        devis:{
                            ...state.devis,
                            loading: false,
                            data: payload
                        }
                    }
        
            case LIST_FACTURE_LOAD:
                return {
                    ...state,
                    factures:{
                        ...state.factures,
                        loading: true,
                        error:null
                    }
                }
            case LIST_FACTURE_SUCCES:
                    return {
                        ...state,
                        factures:{
                            ...state.factures,
                            loading: false,
                            data: payload
                        }
                    }
        default:
            return state;
    }
}

export default file;