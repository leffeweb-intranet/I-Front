import {QUESTION_LOADING,QUESTION_LOADING_SUCCESS,QUESTION_LOADING_ERROR, ADD_QUESTION_LOADING, CLEAR_ADD_QUESTION, ADD_QUESTION_SUCCESS, LIST_QUESTION_LOADING, LIST_QUESTION_LOADING_ERROR, LIST_QUESTION_LOADING_SUCCESS, ADD_QUESTION_ERROR, ADD_ANSWER_LOADING, ADD_ANSWER_SUCCESS, ADD_ANSWER_ERROR, CLEAR_ADD_ANSWER, LIST_ANSWER_SUCCESS, LIST_ANSWER_ERROR, LIST_ANSWER_LOADING } from "../../constants/actionTypes";

const question=(state,{payload,type})=>{
    switch (type){
        case QUESTION_LOADING:
            return {
                ...state,
                question:{
                    ...state.question,
                    loading: true,
                    error:null
                }
            }
           
        case QUESTION_LOADING_ERROR:
                return {
                    ...state,
                    question:{
                        ...state.question,
                        loading: false
                    }
                }

                case QUESTION_LOADING_SUCCESS:

                return{
                    ...state,
                    question : {
                        ...state.question,
                        loading:false,
                        data:payload
                    }
                }



         case ADD_QUESTION_LOADING:
                return{
                    ...state,
                    addQuestion : {
                        ...state.addQuestion,
                        loading:true,
                        error:null
                    }
                }
        case ADD_QUESTION_SUCCESS:
                return {
                    ...state,
                    addQuestion:{
                        ...state.addQuestion,
                        loading: false,
                        data: payload
                    }
                }
        case ADD_QUESTION_ERROR:
                return {
                    ...state,
                    addQuestion:{
                        ...state.question,
                        loading: false
                    }
                }



        case CLEAR_ADD_ANSWER:{
            return{
                ...state,
                answer : {
                    ...state.answer,
                    loading:false,
                    error:null,
                    data:null,
                }
            }
        }

        case CLEAR_ADD_QUESTION:{
            return{
                ...state,
                addQuestion : {
                    ...state.addQuestion,
                    loading:false,
                    error:null,
                    data:null,
                }
            }
        }

        case LIST_QUESTION_LOADING:
            return {
                ...state,
                questions:{
                    ...state.questions,
                    loading: true,
                    error:null
                }
            }
        case LIST_QUESTION_LOADING_ERROR:
                return {
                    ...state,
                    questions:{
                        ...state.questions,
                        loading: false
                    }
                }
        case LIST_ANSWER_SUCCESS:
                return {
                    ...state,
                    answers:{
                        ...state.answers,
                        loading: false,
                        data: payload
                    }
                }
                case LIST_ANSWER_LOADING:
                    return {
                        ...state,
                        answers:{
                            ...state.answers,
                            loading: true,
                            error:null
                        }
                    }
                case LIST_ANSWER_ERROR:
                        return {
                            ...state,
                            answers:{
                                ...state.answers,
                                loading: false
                            }
                        }
                case LIST_QUESTION_LOADING_SUCCESS:
                        return {
                            ...state,
                            questions:{
                                ...state.questions,
                                loading: false,
                                data: payload
                            }
                        }
        case ADD_ANSWER_LOADING:
                return{
                    ...state,
                    addAnswer : {
                        ...state.addAnswer,
                        loading:true,
                        error:null
                    }
                }
        case ADD_ANSWER_SUCCESS:
                return {
                    ...state,
                    addAnswer:{
                        ...state.addAnswer,
                        loading: false,
                        data: payload
                    }
                }
        case ADD_ANSWER_ERROR:
                return {
                    ...state,
                    addAnswer:{
                        ...state.addAnswer,
                        loading: false
                    }
                }
        default:
            return state;
    }
}

export default question;