import { Tabs } from 'antd'
import React from 'react'
import { AppleOutlined, AndroidOutlined } from '@ant-design/icons';
import { Devis } from '../devis/devis';
import { Facture } from '../facture/facture';

const { TabPane } = Tabs;

const Livrable = () => {
    return (

      <Tabs defaultActiveKey="1">
        <TabPane
          tab={
            <span>
              <AppleOutlined />
              Devis
            </span>
          }
          key="1"
        >
          <Devis/>
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              Facture
            </span>
          }
          key="2" >
          <Facture/>
        </TabPane>
      </Tabs>
    )
}

export default Livrable
