import { Card, Col, Row, Space } from "antd";
import './style.css';
import {useState} from 'react';
import {  Select} from 'antd';
import { Input,Switch } from 'antd';
const { TextArea } = Input;


const {Option}= Select;
export const NewMessage=()=>{
   let  handleChange=()=>{
        return ''
    }
    const [input, setInput] = useState(true);

    return(
        <Row>
            <Col span={24}>
                <Card title="Nouveau sujet" bordered={false}>
                    <div className="site-card-wrapper">
                    <Space  size="large" direction="vertical">

                        <Row justify="start"   style={{flexDirection:"column"}}>
                            

                        <h2>De quoi s'agit il?</h2>
                        <Select defaultValue="newDevis" style={{ width: 400 }} onChange={handleChange}>
                            <Option value="newDevis">Demande de devis</Option>
                            <Option value="issue">Problème rencontré</Option>
                        
                            <Option value="rdv">Demande de rendez vous</Option>
                        </Select>
                        
                        </Row>
                        <Row justify="start"   style={{flexDirection:"column"}}>

                        <h2>Urgent?</h2>

                        <Switch
                        checked={input}
                        checkedChildren="Urgent"
                        unCheckedChildren="Pas urgent"
                        onChange={() => {
                            setInput(!input);
                        }}
                        />
                                                </Row>

                        <Row justify="start"   style={{marginTop:"1em",flexDirection:"column"}}>

                        <h2>Message</h2>

                        <TextArea rows={4} />
                        </Row>

                        </Space>
                            
                    </div>
                </Card>
            </Col>
        </Row>
    )
}
 