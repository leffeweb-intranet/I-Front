import { Button, Card, Col, Divider, Row, Space, Tooltip,Form } from "antd";
import './style.css';
import {useState} from 'react';
import {  Select} from 'antd';
import { Input } from 'antd';
import data from './data.json';

import { PaperClipOutlined, SendOutlined, SmileOutlined } from "@ant-design/icons";
import moment from "moment";
import { useForm } from "antd/lib/form/Form";
const { TextArea } = Input;

const {Option}= Select;
export const Conversation=({messages,form,userId})=>{
   let  handleChange=()=>{
        return ''
    }
    const [input, setInput] = useState(true);
    return(
        <>
        <Card  style={{height:"100%",justifyContent:"space-evenly"}} className="messages" title="message"
          span={20}>
         <div className='contentBox' >
             {messages.data.map(elem=><Message userId={userId} message={elem}/>)}

        </div>
        <Form
        onFinish={form.onSubmit}
        >
        <div className='footer' >
          <Form.Item
          style={{width:"100%"}}
          name="content"
          >
          <TextArea   />
          </Form.Item>
        <Space>

        <Tooltip title="Ajouter une icône">

        <Button type="primary" shape="circle" icon={<SmileOutlined />} />
        </Tooltip>

        
        <Tooltip title="Joindre un document">

        <Button type="primary" shape="circle" icon={<PaperClipOutlined />} />
        </Tooltip>

        <Tooltip title="Envoyé">

            <Button type="primary" loading={form.addMessage.loading} htmlType="submit" shape="circle" icon={<SendOutlined />} />
        </Tooltip>

        </Space>
          </div>
          </Form>
           </Card>
        </>
    );

}


const Message=({message,userId})=>{
const date =moment(message.createdAt).format('MM/DD/YYYY à HH:mm');
const me=userId==message.clientId;
console.log(me);
return(
<>

      <div className={me ? 'boxMessage send':'boxMessage receive'} >
        <p>{me ? 'Moi':'LeffeWeb'}  <b>
          {/* {message.author } */}
        </b>  <span>Le {date} </span> </p>
        <Divider />
          <p>{message.content} </p>
      </div>
      </>
)
}
