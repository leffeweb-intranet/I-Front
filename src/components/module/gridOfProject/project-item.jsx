import { Row, Col,Skeleton, Card} from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined  } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import TextTruncate from 'react-text-truncate';
import React from 'react';
const { Meta } = Card;

export  function Project(props){
    const name= props.data.name;
    const description= props.data.description;
    const image= props.data.image;
    const url ="project/"+props.data.id;
    const configure ="project/"+props.data.id+'/prestation';

 return(
     <Col>
        <Card
        style={{ width: 500}}
        actions={[
            <Link to={url} >
            <SettingOutlined key="setting" />
            </Link>,
            <Link to={configure} >
            <EditOutlined key="edit" />
            </Link>,
            <EllipsisOutlined key="ellipsis" />,
        ]}
        >
            <Skeleton loading={props.loading} avatar active>
                <Row justify="space-around">
        
                <Col span={10}>
                    <div style={{backgroundSize:'cover',backgroundPosition:"center",backgroundImage:`url(${image})`,width:190,height:190}} />
                </Col>
                <Col span={12 } >
                    <Meta
                    title={name}
                    description={<TextTruncate
                        line={7}  
                        text={description}
                        />} 
                    />
                </Col>
                </Row>
            </Skeleton>
        </Card>
     </Col>
 );
 }