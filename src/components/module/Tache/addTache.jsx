import { Drawer, Form, Button, Col, Row, Input, Select, DatePicker } from 'antd';
import {  PlusSquareOutlined } from '@ant-design/icons';
const { Option } = Select;

export const AddTache =({visible,showDrawer,onClose,onSubmit})=>{
    const [form] = Form.useForm();
    return (
      <>
        <Button block onClick={showDrawer} >
          <PlusSquareOutlined style={{fontSize:'1em'}}/> 
          Ajouter une tache
        </Button>
        <Drawer
          title=" Créer une nouvelle tâche"
          width={720}
          onClose={onClose}
          visible={visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: 'right',
              }}
            >
              <Button onClick={onClose} style={{ marginRight: 8 }}>
                Annulé
              </Button>
              <Button onClick={() => {
                    form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        onSubmit(values);
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
                }}
                type="primary">
                Enregistrer
              </Button>
            </div>
          }>
          <Form form={form} layout="vertical" hideRequiredMark>

            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="name"
                  label="Titre"
                  rules={[{ required: true, message: 'Vous devez renseigner un titre à votre tâche.' }]}
                >
                  <Input placeholder="Titre de la tâche" />
                </Form.Item>
              </Col>
            
            </Row>
      
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item
                  name="description"
                  label="Description"
                  rules={[
                    {
                      required: true,
                      message: 'Vous devez ajouter une description à votre tâche',
                    },
                  ]}
                >
                  <Input.TextArea rows={4} placeholder="Description de votre tâche" />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Drawer>
      </>
    );
  }
