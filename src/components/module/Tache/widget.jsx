import { Card, Col, Progress, Spin } from "antd"


export const TacheWidget=(taches)=>{
    let {toDo,done,inProgress}=taches.data;
    const data=toDo.concat(done).concat(inProgress);
    const finish=(done.length/data.length)*100;

    return(
        <Card title="Tâche" bordered={false}>
       

            <Progress
                type="circle"
                strokeColor={{
                    '0%': '#108ee9',
                    '100%': '#87d068',
                }}
                percent={finish ?finish.toPrecision(2) :0}
                />

        
        </Card>

    )
    }