import {
  Table,
  Button,
  Row,
  Col,
  Card,
  Badge,
  Progress,
  Space,
  Image,
  Tag,
} from "antd";
import { useState } from "react";
import { NavLink } from "react-router-dom";
import "./assets/style.css";
import Icon, {
  StepForwardOutlined,
  TableOutlined,
  StepBackwardOutlined,
} from "@ant-design/icons";
import TextTruncate from "react-text-truncate";
import kabanIcon from "./assets/kabanIcon.svg";
import { AddTache } from "./addTache";

export const Tache = ({ addProject, taches, changeStatus }) => {
  const [kabanView, setkabanView] = useState(false);
  const handleView = () => {
    setkabanView(!kabanView);
  };

  return (
    <div className="site-card-wrapper">
      <Row gutter={[16, 16]}>
        <Col lg={15} md={24}>
          <Button onClick={handleView}>
            {kabanView ? (
              <Image preview={false} width={30} src={kabanIcon} />
            ) : (
              <TableOutlined sizes="large" />
            )}
          </Button>
          {kabanView ? (
            <Card style={{ marginTop: "1em" }} title="Tâche" bordered={false}>
              <TableTache {...taches.data} />
            </Card>
          ) : (
            <Kaban {...taches.data} changeStatus={changeStatus} />
          )}
        </Col>
        <Col lg={6} md={24}>
          <Informations {...taches.data} />
          <div style={{ marginTop: "1em" }}>
            <AddTache {...addProject} />
          </div>
        </Col>
      </Row>
    </div>
  );
};

const Informations = ({ toDo, inProgress, done }) => {
  const data = toDo.concat(inProgress).concat(done);
  console.log(data);

  console.log(data.length);
  return (
    <Card title="Informations" bordered={false}>
      <h3> À faire ({toDo.length}) </h3>
      <Progress
        percent={Math.round((toDo.length / data.length) * 100)}
        size="small"
        strokeColor="#1890ff"
        status="active"
      />

      <h3> En cours ({inProgress.length}) </h3>
      <Progress
        percent={Math.round((inProgress.length / data.length) * 100)}
        size="small"
        strokeColor="#faad14"
        status="active"
      />

      <h3>Tâche finis({done.length}) </h3>
      <Progress
        percent={Math.round((done.length / data.length) * 100)}
        size="small"
        strokeColor={{ "0%": "#108ee9", "100%": "#87d068" }}
        status="active"
      />
    </Card>
  );
};

const columns = [
  {
    title: "Titre",
    dataIndex: "name",
  },
  {
    title: "Status",
    dataIndex: "status",
    render: (status) => {
      let text;
      let color;

      switch (status) {
        case "inProgress":
          text = "En cours";
          color = "rgb(250, 173, 20)";

          break;
        case "toDo":
          text = "À faire ";
          color = "rgb(24, 144, 255)";

          break;
        default:
          text = "Terminé";
          color = "#389e0d";
          break;
      }

      return <Tag color={color}>{text}</Tag>;
    },
  },
  {
    title: "Description",
    dataIndex: "description",
  },
];

const TableTache = ({ toDo, inProgress, done }) => {
  const data = toDo.concat(inProgress).concat(done);

  const setStatusSort = () => {
    setSortedInfo({
      order: "descend",
      columnKey: "age",
    });
  };

  const [filteredInfo, setFilteredInfo] = useState(null);
  const [sortedInfo, setSortedInfo] = useState(null);

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loading, setLoading] = useState(false);

  const start = () => {
    setLoading(true);

    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(true);
    }, 1000);
  };

  const onSelectChange = (selectedRowKeys) => {
    setSelectedRowKeys({ selectedRowKeys });
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
  return (
    <div>
      <div style={{ marginBottom: 16 }}>
        <span style={{ marginLeft: 8 }}>
          {hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}
        </span>
      </div>
      <Table columns={columns} dataSource={data} />
    </div>
  );
};

const Kaban = ({ toDo, inProgress, done, changeStatus }) => {
  console.log("toDo", toDo);
  return (
    <Row style={{ marginTop: "1em" }}>
      <div className="kaban">
        <Card className="kabanCol do" title={`À faire (${toDo.length})`}>
          <Space direction="vertical">
            {toDo.map((elem) => (
              <Card
                className="kabanItem"
                title={elem.name}
                actions={[
                  <Button onClick={() => changeStatus(elem, "inProgress")}>
                    <StepForwardOutlined />
                  </Button>,
                ]}
              >
                <TextTruncate line={3} text={elem.description} />
              </Card>
            ))}
          </Space>
        </Card>

        <Card
          className="kabanCol progress"
          title={`En cours (${inProgress.length})`}
        >
          <Space direction="vertical">
            {inProgress.map((elem) => (
              <Card
                className="kabanItem"
                className="kabanItem"
                title={elem.name}
                actions={[
                  <Button onClick={() => changeStatus(elem, "toDo")}>
                    <StepBackwardOutlined />
                  </Button>,
                  <Button onClick={() => changeStatus(elem, "Done")}>
                    <StepForwardOutlined />
                  </Button>,
                ]}
              >
                <TextTruncate line={3} text={elem.description} />
              </Card>
            ))}
          </Space>
        </Card>

        <Card className="kabanCol don" title={`Terminé (${done.length})`}>
          <Space direction="vertical">
            {done.map((elem) => (
              <Card
                className="kabanItem"
                title={elem.name}
                actions={[
                  <Button onClick={() => changeStatus(elem, "inProgress")}>
                    <StepBackwardOutlined />
                  </Button>,
                ]}
              >
                <TextTruncate line={3} text={elem.description} />
              </Card>
            ))}
          </Space>
        </Card>
      </div>
    </Row>
  );
};
