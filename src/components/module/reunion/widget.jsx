import { VideoCameraOutlined} from "@ant-design/icons"
import { Button, Card , Space, Table} from "antd"
import moment from "moment";
import data from  './data.json'

export const ReuWidget=({reunions})=>{

    let reu=data.reunions.map(reu=>{ 
        return { heure:`À ${reu.heure}`,
            duree:` durée: ${reu.duree}`,
            link:<Space>
                <Button type='primary' targe='_blank' href={reu.link}> Rejoindre</Button>
                <Button type='primary' style={{background:'red',border:'0'}} targe='_blank' href={reu.link}> Cancel</Button>

                </Space>,}
    });
    const columns = [
        {
          title: 'Heure',
          dataIndex: 'hours',
          render:(text,obj)=>{
              console.log(text,obj);
              const dateTarget=`${obj.date} ${text}`;
            const time=moment(dateTarget);
            const now=moment(new Date());
            const duration=moment.duration(now.diff(time));
            const Inhours=duration.asHours().toPrecision(2);
            if (Inhours<0){
                return <p> Dans {-Inhours} heures</p>
            }else{
                return <p> Il y à {Inhours} heures</p>
            }
        }

        },
        {
            liens: 'Durée',
            dataIndex:'duration',
            render:(text)=><p>{text} min</p>
        },    
        {
            liens: 'Lien',
            dataIndex:'link',
            render: (text)=><Button style={{backgroundColor:"#1890ff",color:"white"}}  icon={<VideoCameraOutlined/>}>Ouvrir</Button>
        }
    
    ]
        ;
    return(
        <Card title="Réunion à venir">

        <Table showHeader={false} columns={columns} loading={reunions.loading} dataSource={reunions.data} />

        </Card>

    )

}