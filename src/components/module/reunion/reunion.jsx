import { Card, Col, Row, Space,Table ,DatePicker, Button, Tag} from "antd";
import {useState} from 'react';
import {CloseOutlined, VideoCameraOutlined} from '@ant-design/icons';
import { Input,Switch } from 'antd';
import {AddReunion} from './addReunion';
import dataReu from './data.json';
import { NavLink } from "react-router-dom";

export const Reu =({onSubmit,reunions})=>{
    return (
    <Row direction="horizontal">
      <Space  wrap={true} align="start"  size={25}>
    <Card title="Mes réunions plannifiés" bordered={false} >
        <TableReu reunions={reunions}/>
    </Card>
    <AddReunion onSubmit={onSubmit}/>
    </Space>

    </Row>);
}

const columns = [
    {
      title: 'Motif',
      dataIndex: 'name',
      render: motif =>{
        let  text;
        let  color;
  
        switch  (motif){
          case 'issue':
            text="Problème rencontré"
            color="rgb(30, 173, 20)";
  
            break;
        default:
          text="Demande de préstation";
          color="#389e0d";
          break
        }
        
        return <Tag
      color={color}>
        {text}</Tag>
      }
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: status =>{
        let  text;
        let  color;
  
        switch  (status){
          case 'inProgress':
            text="En cours"
            color="rgb(250, 173, 20)";
  
            break;
        case 'toDo':
            text="Prévu "
            color="rgb(24, 144, 255)";
  
            break;
        default:
          text="Passé";
          color="#389e0d";
          break
        }
        
        return <Tag
      color={color}>
        {text}</Tag>
      }
    },
    {
      title: 'Desciption',
      dataIndex: 'desciption',
    },
    {
        title : 'Date',
        dataIndex : 'date'
    },
    {
        title : 'Heure',
        dataIndex : 'hours'
    },
    {
        title : 'Durée',
        dataIndex : 'duration'
    },
    {
        title : 'Lien',
        dataIndex : 'url',
        render: (text)=><NavLink to={text}><Button style={{backgroundColor:"#1890ff",color:"white"}}  icon={<VideoCameraOutlined/>}>Ouvrir</Button></NavLink>
    }
    ,
    {
        title : 'Annulé',
        render:(text)=><Button style={{backgroundColor:"#EB0258",color:"white"}} icon={<CloseOutlined />}> Annulé </Button>
    }
  ];


const TableReu=({reunions})=> {
    const [selectedRowKeys,setSelectedRowKeys]= useState([]);
    const [loading,setLoading]= useState(false);
    const  start = () => {
      setLoading(true);
      setTimeout(() => {
          setSelectedRowKeys([]);
          setLoading(true);
      }, 1000);
    };
  
    const onSelectChange = selectedRowKeys => { setSelectedRowKeys({ selectedRowKeys }); };
      const rowSelection = {
        onChange: onSelectChange
      };
      return (  
          <Table  columns={columns} loading={reunions.loading} dataSource={reunions.data} />
      );
  }
  
  const GridReu=({reu})=>{
      return(
          <Card title={reu.title}>
                        <Table  columns={columns} dataSource={reu.data} />
          </Card>
      )
  }