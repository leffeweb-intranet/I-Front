import React from "react";
import Layout, { Content, Header } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import { LeftMenu } from "./partial/LeftMenu";
import "./assets/style.css";
import TopMenu from "./partial/TopMenu";
import { Drawer } from "antd";
import { withAuth0} from "@auth0/auth0-react";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  CaretRightOutlined,
} from "@ant-design/icons";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { projectRoutes } from "../../../routes";

class ProjectDashboard extends React.Component {
  render() {
    return (
      <>
        <Router>
          <Layout id="allLayout">
            {
              <Sider
                trigger={null}
                collapsible
                collapsed={this.state.collapsed}
              >
                <LeftMenu
                  toggle={this.toggle}
                  setSelection={this.setSelection}
                  {...this.state}
                />
              </Sider>
            }

            <Layout className="site-layout">
              <Header className="site-layout-background" style={{ padding: 0 }}>
                <div className="headerLeft">
                  {React.createElement(
                    this.state.collapsed
                      ? MenuUnfoldOutlined
                      : MenuFoldOutlined,
                    {
                      className: "trigger",
                      onClick: this.toggle,
                    }
                  )}

                  <h1>
                    Project name
                    <CaretRightOutlined /> {this?.state?.selection}
                  </h1>
                </div>
                <TopMenu
                  toggle={this?.toggle}
                  showDrawer={this?.showDrawer}
                  {...this?.props}
                />
              </Header>

              <Content className="site-layout-background">
                <Drawer
                  title="Aide"
                  placement="right"
                  closable={false}
                  onClose={() => this.onClose()}
                  visible={this.state.helper}
                >
                  <HelperRigth />
                </Drawer>
                <Switch>
                  {projectRoutes.map((route, index) => (
                    <route.component {...route} key={index} />
                  ))}
                </Switch>
              </Content>
            </Layout>
          </Layout>
        </Router>
      </>
    );
  }
}

const HelperRigth = ({ content }) => {
  return <h1>Aide</h1>;
};

export default withAuth0(ProjectDashboard);
