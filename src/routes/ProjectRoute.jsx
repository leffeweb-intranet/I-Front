import React from 'react'
import {projectRoutes} from './index'
const ProjectRoute = () => {
    
    return projectRoutes.map((route,index)=> (
        <route.component {...route} key={index}/>
                ))

}

export default ProjectRoute
