import { Row } from "antd"
import Layout from "antd/lib/layout/layout"


export  const ErrorDist=()=>(

    <Layout style={{background:'#001529'}}>

        <Row  justify='center' style={{margin:'auto'}}>
            <h1 style={{border:"1px solid white", padding:"1em",color:"white",textAlign:'center'}}> <b >Nous sommes désolé.</b><br/> Une erreur à était rencontré avec le serveur. <br/> Merci de réessayer plus tard </h1>
        </Row>
    </Layout>
)


