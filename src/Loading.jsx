import { Row, Spin } from "antd";
import Layout from "antd/lib/layout/layout";

export const Loading = ()=>{
    return(
        <Layout style={{background:'#001529'}}>
            <Row style={{margin:"auto"}}>
            <Spin size='large' style={{color:'white'}} tip="Lancement de votre espace..."/>
            </Row>
     
    </Layout>

    )
}