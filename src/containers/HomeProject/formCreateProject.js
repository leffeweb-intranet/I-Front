import { useAuth0 } from "@auth0/auth0-react";
import { useContext } from "react"
import {useHistory} from "react-router-dom";
import { AddProject } from "../../components/screen/home/addProject";
import { createProject } from "../../context/actions/project/createProject";
import {GlobalContext} from "../../context/Provider";

export default ()=>{    
    const  history=useHistory();
    const {
        projectDispatch,
        projectState:{
            addProject :{
                loading,
                error,
                data
                }
        },
        userDispatch,
        userState:{
            user: {
                data:client
            }
        }
    } = useContext(GlobalContext);

     const  onSubmit=(values)=>{
        values.client=client;
        createProject(values)(projectDispatch)
    }
    return {onSubmit,loading,data}
}