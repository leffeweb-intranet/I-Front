import { useContext, useEffect, useState } from "react"
import { useHistory } from "react-router";
import Home from "../../components/screen/home"
import { getProjects } from "../../context/actions/project/getProjects";
import clearListProject from "../../context/actions/project/clearListProject";
import { GlobalContext } from "../../context/Provider"
import { useAuth0 } from '@auth0/auth0-react';
import formCreateProject from "./formCreateProject";
import { getToken } from "../../context/actions/auth/getScope";
import { userInformation } from "../../context/actions/user/userInformation";

 const HomeProjectContainer = () =>{
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

    const  history=useHistory();
    const {
        projectState:{
            projects:{
                data,
                loading,
                error
            }
        },
        projectDispatch,
        userState:{
            user:client
        },
        userDispatch,
    } =  useContext(GlobalContext);
     
    useEffect(async () => {
        getToken('read:projects',getAccessTokenSilently,getAccessTokenWithPopup);
    },[])
    
    const formAddProject=formCreateProject();

    useEffect(() => {
        userInformation(user.sub)(userDispatch);
        
    },[])

    console.log('CLIENNNT',client);
    if(client?.data?.firstConnexion){
        history.push('/premiere-connexion')
    }
    useEffect(() => {
            getProjects(user.sub)(projectDispatch);
            return ()=>{
                clearListProject(projectDispatch)
            }
    },[formAddProject.data])

    if(false==client.data){
        history.push('/premiere-connexion')
    }
    console.log('projects',data);

    const [modal, setModal] = useState(false);
    const showModal = () => { setModal(true) };
    const closeModal = () => { setModal(false) };
    console.log('location',client);

    return  <Home 
            client={client}    
            modal={{showModal,modal,closeModal}} 
            logout={logout} 
            projects={{data,loading,error}} 
            formAddProject={formAddProject} />;
}

export default HomeProjectContainer;