

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect } from 'react'
import { getProject } from '../../context/actions/project/getProject';
import {  userInformation } from '../../context/actions/user/userInformation';

import { getMessages } from '../../context/actions/message/getMessages';
import { createMessage } from '../../context/actions/message/createMessage';

import { GlobalContext } from '../../context/Provider';
import { Design } from '../../components/module/design/design';

export const ProjectLivrableContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client
        },
        userDispatch,
        messageState:{
          addMessage,
          messages
        },
        messageDispatch
    } =  useContext(GlobalContext);
     
     const {projectId} =props.match.params;
    /* REQUEST */
    useEffect(() => {
      getProject(projectId)(projectDispatch);
    }, []);

    useEffect(() => {
        userInformation(user.sub)(userDispatch);
      }, []);
    
    useEffect(() => {
      getMessages(projectId)(messageDispatch);
    }, [addMessage.loading]);
    console.log(messages);

    const onSubmit=(values)=>{
        values.client=client?.data;
        values.project=project?.data;

        console.log(values);
        createMessage(values)(messageDispatch);
    }

    const userId=user.data?.id;
    return    <Design  
                form={{addMessage,onSubmit}}
                messages={messages}
                userId={userId}
                  />
}
