import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext } from 'react'
import { useHistory } from 'react-router';
import { FormFirst } from '../../components/screen/formFirst/formFirst'
import { editUserInformation } from '../../context/actions/user/editUserInformation';
import { GlobalContext } from '../../context/Provider';

export const FirstFormContainer = () => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();
    const  history=useHistory();
        const {
            userDispatch,
            userState:{
                editUser
            }
    } = useContext(GlobalContext);


    const onSubmit=(values)=>{
        values.idAuth=user.sub
    editUserInformation(values)(userDispatch)
    }

    if(editUser.data){
        history.push('/')
    }
    return <FormFirst
    onSubmit={onSubmit}
    />
}

export default FirstFormContainer
