

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import {ProjectDashboardUI} from '../../components/screen/ProjectDashboard/projectDashboardUI'
import { getProject } from '../../context/actions/project/getProject';
import { userInformation } from '../../context/actions/user/userInformation';
import { GlobalContext } from '../../context/Provider';

export const ProjectDashboardContainer = (props) => {
    const { user } = useAuth0();
      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client
        },
        userDispatch,  
    } =  useContext(GlobalContext);
     
    const [collaspeMenu, setcollaspeMenu] = useState(false);
    const toggle=()=>{
      setcollaspeMenu(!collaspeMenu);
    }
     const {projectId} =props.match.params;

    /* REQUEST */

    useEffect(() => {
      getProject(projectId)(projectDispatch);
    }, []);

    useEffect(() => {
      userInformation(user.sub)(userDispatch);
    }, []);

    return <ProjectDashboardUI
    user={client}
    projectId={projectId}
    collapse={{collaspeMenu,setcollaspeMenu,toggle}}
    project={project}  
  
    />
}
