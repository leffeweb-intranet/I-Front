export default {
    reunion: {
        loading: false,
        data: null,
        error: null
    },
    addReunion: {
        loading: false,
        data: null,
        error: null
    },
    reunions: {
        loading: false,
        data: [],
        error: null
    },

}