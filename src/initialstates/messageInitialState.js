export default {
    message: {
        loading: false,
        data: null,
        error: null
    },
    addMessage: {
        loading: false,
        data: null,
        error: null
    },
    messages: {
        loading: false,
        data: [],
        error: null
    },

}